# Big data sort

This is the big data project for class CSC550-1

## Steps to setup repo

`git clone https://shwetachiddarwar@bitbucket.org/shwetachiddarwar/big-data.git`

`cd big-data\BigDataSort`

Download Maven - https://www-eu.apache.org/dist/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.zip

Open `DataGenerator.java` class in an editor and comment/uncomment line number 21/24 as per the guideline mentioned on line 20 and 23.

LOCATION_TO_MAVEN_DIRECTORY\apache-maven-3.6.1\bin\mvn clean package

Above `mvn clean package` command will generate bigdatasort.jar file in target directory. This is our file of interest.

## Steps to run

### Pre-requisite: 
1. Make sure you have setup hadoop as per the guidelines shared by Professor or follow it from hadoop website.

2. Make sure you have updated these files as per hadoop setup guideines from website-
`hadoop-env.sh`, `core-site.xml`, `mapred-site.xml`, `hdfs-site.xml`

### Commands:

`hdfs dfs -mkdir /user/`
`hdfs dfs -mkdir /user/hadoop`
`hdfs dfs -mkdir /user/hadoop/sort`
`hdfs dfs -mkdir /user/hadoop/sort/input`

#### generate data: 
`java -cp BigData/BigDataSort/target/bigdatasort.jar edu.itu.se.bigdata.generate.DataGenerateDriver`

#### copy generated data from local to hdfs: 
`hadoop fs -copyFromLocal /tmp/bigDataOfBusinesses.csv /user/hadoop/sort/input/`

#### run map-reduce on generated data: 
`hadoop jar BigData/BigDataSort/target/bigdatasort.jar edu.itu.se.bigdata.sort.DataSortDriver /user/hadoop/sort/input/ /user/hadoop/sort/output/`

#### run validation on sorted data: 
`hadoop jar BigData/BigDataSort/target/bigdatasort.jar edu.itu.se.bigdata.validate.DataValidateDriver`

### Result:
Hadoop installed on MacBook pro with 16 GB Memory and 2.8 GHz Intel Core i7 processor:

Data generation - 4 minutes

Data sort - 13 minutes

Data validation - 4 minutes 