package edu.itu.se.bigdata.sort;

import com.opencsv.CSVParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class DataSort {
    public static class DataSortMapper extends Mapper<LongWritable,
            Text, Text, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException,
                InterruptedException {
            try {
                CSVParser parser = new CSVParser();
                String[] lines = parser.parseLine(value.toString());
                context.write(new Text(lines[3]), new Text(value.toString()));
            } catch (Exception e) {}
        }
    }

    public static class DataSortReducer extends Reducer<Text,
            Text, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            for(Text v : values){
                context.write(null, v);
            }
        }
    }
}
