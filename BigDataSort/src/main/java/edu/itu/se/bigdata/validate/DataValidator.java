package edu.itu.se.bigdata.validate;

import java.io.IOException;
import java.io.InterruptedIOException;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;


public class DataValidator extends Configured{

    public static class PostValidatorMapper extends
            Mapper<LongWritable, Text, Text, Text> {
        String previousLn = null;

        public void map(LongWritable key, Text value, Mapper.Context context)
                throws IOException, InterruptedException {
            if (value != null
                    && value.toString().length() > 0
                    && value.toString().contains(",")) {
                String currentLn = value.toString();
                fieldCount(currentLn, context);
                if (previousLn != null
                        && currentLn != null) {
                    compare(previousLn, currentLn, context);
                }
                previousLn = currentLn;
            } else {
                StringBuilder data = new StringBuilder();
                String fileName = ((FileSplit)(context).getInputSplit()).getPath().getName();
                data.append("Post sorting validation failed")
                        .append(String.format("Invalid data", fileName, ",", ((value != null) ? value.toString() : value)));
                throw new InterruptedIOException(data.toString());
            }
        }

        private void fieldCount(String currentLine, Mapper.Context context) throws InterruptedIOException {
            String[] fields = currentLine.split(",");
            if (fields != null
                    && fields.length > 0
                    && fields.length == 6) {
                // do nothing
            } else {
                StringBuilder data = new StringBuilder();
                String fileName = ((FileSplit)(context).getInputSplit()).getPath().getName();
                data.append("Post sorting validation failed")
                        .append(" Data is inconsistent "+ ", " + fileName +", "+ currentLine);
                throw new InterruptedIOException(data.toString());
            }
        }

        private void compare(String previousLn, String currentLn, Context context) throws InterruptedIOException {
            boolean isValid = false;
            String[] prevFields = previousLn.split(",");
            String[] currFields = currentLn.split(",");

            if (prevFields != null
                    && prevFields.length >= 2
                    && currFields != null
                    && currFields.length >= 2) {
                isValid = ((prevFields[3].compareTo(currFields[3]) <= 0));
            }

            if (!isValid) {
                StringBuilder data = new StringBuilder();
                String fileName = ((FileSplit)(context).getInputSplit()).getPath().getName();
                data.append("Post sort validation failed")
                        .append(" Invalid sorted data"+", " +fileName+", "+ previousLn+", "+currentLn);
                throw new InterruptedIOException(data.toString());
            }
        }
    }
}