package edu.itu.se.bigdata.validate;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

public class DataValidateDriver {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        long start = System.currentTimeMillis();
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        job.setJobName("DataValidation");
        job.setJarByClass(DataValidateDriver.class);
        FileInputFormat.setInputPaths(job, new Path("/user/hadoop/sort/output/"));
        job.setMapperClass(DataValidator.PostValidatorMapper.class);
        job.setNumReduceTasks(0);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileOutputFormat.setOutputPath(job, new Path("/user/hadoop/sort/validatedOutput/"));
        job.waitForCompletion(true);
        System.out.println("Time to validate data " + (System.currentTimeMillis() - start) + " ms");
    }
}
