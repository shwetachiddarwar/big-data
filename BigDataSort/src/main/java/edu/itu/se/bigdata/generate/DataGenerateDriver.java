package edu.itu.se.bigdata.generate;

public class DataGenerateDriver implements Runnable
{
    public static void main( String[] args )
    {
        try{
            Thread t1 = new Thread(new DataGenerateDriver());
            t1.start();
        }catch(Exception ioe){
            ioe.printStackTrace();
        }
    }

    @Override
    public void run() {
        try{
            System.out.println("Started thread - "+ Thread.currentThread().getId());
            DataGenerator.getInstance().generateBigData();
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            DataGenerator.closeWriter();
        }
    }
}
