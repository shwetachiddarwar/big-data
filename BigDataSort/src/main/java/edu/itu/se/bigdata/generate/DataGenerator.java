package edu.itu.se.bigdata.generate;

import org.fluttercode.datafactory.impl.DataFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DataGenerator {
    int count;
    public static File bigData = null;
    public static FileWriter writer = null;
    public static DataFactory df;
    long start, end;
    public static DataGenerator dataGenerator=null;
    private DataGenerator(){
        try{
        count = 147500000; //147500000 for single thread
        
        //Use this for mac and linux (make sure to comment line 24 if using this) 
        //bigData = new File ("/tmp/bigDataOfBusinesses.csv");
        
        //Use this for windows
        bigData = new File ("bigDataOfBusinesses.csv");
        
        writer = new FileWriter(bigData, true);
        df = new DataFactory();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static DataGenerator getInstance(){
        if(dataGenerator==null){
            synchronized (DataGenerator.class){
                dataGenerator = new DataGenerator();
            }
        }
        return dataGenerator;
    }

    public static void closeWriter(){
        try {
            writer.close();
            Thread.currentThread().stop();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void generateBigData() throws IOException{
        start = System.currentTimeMillis();
        System.out.println("Starting to generate data...."+Thread.currentThread().getId());
        synchronized (dataGenerator){
            for(int i=0;i<count;i++) {
                writer.write(df.getFirstName()+",");
                writer.write(df.getLastName()+",");
                writer.write(df.getBusinessName()+",");
                writer.write(df.getCity()+",");
                writer.write(df.getEmailAddress()+",");
                writer.write(df.getNumberText(10) + "\n");
            }
        }
        end = System.currentTimeMillis();
        System.out.println("Data generation completed...."+Thread.currentThread().getId());
        System.out.println("Time taken to generate file - " +
                (end - start)/1000 + "seconds");
        System.out.println("Generated file size is - "+bigData.length()/1073741824 + " GB");
    }
}
